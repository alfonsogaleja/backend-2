package com.techu.backend.controller;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.service.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    //Endpoint default
    @GetMapping("")
    public String index(){
        return "API REST Tech U! v2.0.0";
    }

    // READ ALL
    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    // READ ID
    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoById(@PathVariable String id){
        return productoService.findById(id);
    }

    // CREATE
    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel nuevoProducto){
        productoService.save(nuevoProducto);
        return nuevoProducto;
    }

    // PUT
    @PutMapping("/productos")
    public void putProducto(@RequestBody ProductoModel nuevoProducto){
        productoService.save(nuevoProducto);
    }

    // DELETE
    @DeleteMapping("/productos")
    public boolean delete(ProductoModel producto){
        return productoService.delete(producto);
    }
}
