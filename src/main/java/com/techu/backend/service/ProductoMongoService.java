package com.techu.backend.service;

import com.techu.backend.model.ProductoModel;
import com.techu.backend.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoMongoService {

    @Autowired
    ProductoRepository productoRepository;
    
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    public Optional<ProductoModel> findById(String id){
        return productoRepository.findById(id);
    }

    public ProductoModel save(ProductoModel nuevoProducto){
        return productoRepository.save(nuevoProducto);
    }

    public boolean delete(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
